Ext.define('Kitchensink.profile.Phone', {
    extend: 'Kitchensink.profile.Base',

    config: {
        controllers: ['Main'],
        views: ['Main', 'TouchEvents']
    },

    isActive: function() {
        return Ext.os.is.Phone; // || Ext.os.is.Desktop;
    },

    launch: function() {
           var mainView=  Ext.create('Kitchensink.view.Login', { id: 'Login' });
           Ext.Viewport.add(mainView);
           this.callParent();
    }
});
