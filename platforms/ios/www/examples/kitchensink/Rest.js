Ext.define('Rest', {
    restServer:'http://localhost/fo/service',
   singleton:true,
   updateUserCredit:function(adjustment){
       var restRrl=Rest.restServer+'/index.php/Home/User/update?openid='+MyAccount.user.wechat_openid+'&adjust='+adjustment;
       MyAccount.user.gongDe=parseInt(MyAccount.user.gongDe)+parseInt(adjustment);
       Ext.Ajax.request({
           url:restRrl ,
           success: function(response) {
           },
           failure: function() {
           }
       });
       var message=adjustment>0?'增加'+adjustment+'积分':'扣除'+(-adjustment)+'积分';
      Rest.showNotice(message);
   },
    updateUserSettings:function(userSettings){
//        var restRrl=Rest.restServer+'/index.php/Home/User/saveSetting?openid='+MyAccount.user.wechat_openid;
        var restRrl=Rest.restServer+'/index.php/Home/User/saveSetting?openid=o3xKBt5SK3OFwAVbf9AfqMi4fh78';
        Ext.Ajax.request({
            url:restRrl ,
            params:userSettings,
            success: function(response) {
            },
            failure: function() {
            }
        });
        var message="保存成功";
        Rest.showNotice(message);
    },
    showNotice:function(message){
        Ext.getCmp('noticeView').setHtml(message);
        var animation = new Ext.Anim({
            easing: 'easeIn',
            duration: 3000,
            autoClear: false,
            delay:200,
            from: {
                opacity:1
            },
            to:{
                opacity:0
            },
            before:function(){
            },
            after:function(){
            }
        });
        animation.run(Ext.get('noticeView'));
    },
    getQueryStringByName:function (name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },
    syncUserInfo:function(){
        var code=Rest.getQueryStringByName('code');
        var homepage=Rest.getQueryStringByName('homepage');
//        var panel = Ext.getCmp('mainFoView');
        MyAccount.user={wechat_openid:'null',username:'未知',gongDe:0};
//        panel.getParent().setMasked({
//            xtype: 'loadmask',
//            message: '加载中...'
//        });

        Ext.Ajax.request({
            url: Rest.restServer+'index.php/Home/User/syncFromWeiXin?code='+code,
            success: function(response) {
               if( response.responseText.indexOf('user_id')==-1)
               {
                   Ext.Msg.alert("错误","获取微信号失败。");
               }
                MyAccount.user=eval("(" + response.responseText + ")");
                var mainView = Ext.create('fo.view.MainView');
                Ext.Viewport.add(mainView);
                var foxiangList = Ext.create('fo.view.InlineDataView',{id:'foxiangList'});
                Navigation.showView('flowerListView', 'fo.view.FlowerListView');
                Navigation.showView('gongXiangView', 'fo.view.GongXiangListView');
                Navigation.showView('lampOilListView', 'fo.view.LampOilListView');
                Navigation.showView('fruitListView', 'fo.view.FruitListView');
                Ext.Viewport.add(foxiangList);
                Ext.Viewport.setActiveItem(mainView);
                Ext.getCmp('noticeView').setHtml('欢迎 '+MyAccount.user.username );
                var animation = new Ext.Anim({
                    easing: 'easeIn',
                    duration: 3000,
                    autoClear: false,
                    delay:200,
                    from: {
                        opacity:1
                    },
                    to:{
                        opacity:0
                    },
                    before:function(){
                    },
                    after:function(){
                    }
                });
                animation.run(Ext.get('noticeView'));
                if(homepage=="dingke"){
                    Navigation.pushOrActivateView('zaoWanKeView','fo.view.ZaoWanKeView');
                }
                setTimeout(function(){
                    Ext.fly('appLoadingIndicator').destroy();
                },200);

            },
            failure: function() {
                Ext.Msg.alert("错误","获取微信号失败。");
                var mainView = Ext.create('fo.view.MainView');
                Ext.Viewport.add(mainView);
                var foxiangList = Ext.create('fo.view.InlineDataView',{id:'foxiangList'});
                Navigation.showView('flowerListView', 'fo.view.FlowerListView');
                Navigation.showView('gongXiangView', 'fo.view.GongXiangListView');
                Navigation.showView('lampOilListView', 'fo.view.LampOilListView');
                Navigation.showView('fruitListView', 'fo.view.FruitListView');
                Ext.Viewport.add(foxiangList);
                Ext.Viewport.setActiveItem(mainView);
                if(homepage=="dingke"){
                    Navigation.pushOrActivateView('zaoWanKeView','fo.view.ZaoWanKeView');
                }
                setTimeout(function(){
                    Ext.fly('appLoadingIndicator').destroy();
                },200);


            }
        });

    }

});