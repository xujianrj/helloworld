/**
 * Demonstrates a tabbed form panel. This uses a tab panel with 3 tabs - Basic, Sliders and Toolbars - each of which is
 * defined below.
 *
 * See this in action at http://dev.sencha.com/deploy/sencha-touch-2-b3/examples/kitchensink/index.html#demo/forms
 */
Ext.define('Kitchensink.view.Login', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.form.FieldSet',
        'Ext.field.Number',
        'Ext.field.Spinner',
        'Ext.field.Password',
        'Ext.field.Email',
        'Ext.field.Url',
        'Ext.field.DatePicker',
        'Ext.field.Select',
        'Ext.field.Hidden',
        'Ext.field.Radio'
    ],
    id: 'basicform',
    config: {
        items: [
            {
                xtype: 'fieldset',
                id: 'fieldset1',
                title: 'Personal Info',
                instructions: 'Please enter the information above.',
                defaults: {
                    labelWidth: '35%'
                },
                items: [
                    {
                        xtype         : 'textfield',
                        name          : 'name',
                        label         : 'Name',
                        placeHolder   : 'Tom Roy',
                        autoCapitalize: true,
                        required      : true,
                        clearIcon     : true
                    },
                    {
                        xtype: 'passwordfield',
                        name : 'password',
                        label: 'Password'
                    }
                ]
            },
            {
                xtype: 'container',
                defaults: {
                    xtype: 'button',
                    style: 'margin: .5em',
                    flex : 1
                },
                layout: {
                    type: 'hbox'
                },
                items: [
                    {
                        text: 'Login',
                        scope: this,
                        hasDisabled: false,
                        handler: function(btn){
                        var main= Ext.create('Kitchensink.view.phone.Main');
                        Ext.Viewport.add(main);
                        Ext.Viewport.setActiveItem(main);

//                            var fieldset1 = Ext.getCmp('fieldset1'),
//                                fieldset2 = Ext.getCmp('fieldset2');
//
//                            if (btn.hasDisabled) {
//                                fieldset1.enable();
//                                fieldset2.enable();
//                                btn.hasDisabled = false;
//                                btn.setText('Disable fields');
//                            } else {
//                                fieldset1.disable();
//                                fieldset2.disable();
//                                btn.hasDisabled = true;
//                                btn.setText('Enable fields');
//                            }
                        }
                    },
                    {
                        text: 'Forget',
                        handler: function(){
                        window.location.href='indexAudio.html';
                        }
                    }
                ]
            }
        ]
    }
});
